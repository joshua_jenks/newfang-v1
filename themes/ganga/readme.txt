=== Ganga ===
Contributors: wpkoithemes
Tags: two-columns, three-columns, one-column, right-sidebar, left-sidebar, footer-widgets, blog, e-commerce, flexible-header, full-width-template, custom-header, custom-background, custom-menu, custom-colors, sticky-post, threaded-comments, translation-ready, featured-images
Requires at least: WordPress 5.0
Requires PHP: 7.0
Tested up to: WordPress 5.7.2
Stable tag: 1.2.0
License: GNU General Public License v3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Ganga is a WPKoi theme under GPL license. Ganga has a clean, pink style with fashion content.

== Description ==

Ganga is a WPKoi theme under GPL license. You can find theme demos at wpkoi.com. Ganga has a colorful, happy style. The theme is multipurpose, so if You like the style, You can use it to represent Your art, business, an event, show Your portfolio, start a blog, start a webshop and sell Your products etc. The demos use Elementor, WooCommerce and Contact Form 7 plugins. The premium demo uses the Ganga premium plugin also. Ganga has an easy to use admin with a lot of customizer functions, so You can build Your dream without coding knowledge. The theme is responsive so looks good on almost every devices. You can setup multiple header styles, navigation styles and layouts. Build You site with Ganga Theme and share it with the world! You can also find a documentation on wpkoi.com.


== Frequently Asked Questions ==

= Installation =

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Ganga in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.

== Copyright ==

Ganga, Copyright (C) 2018, wpkoi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

== Resources ==

The following images, icons, fonts or other files were use as listed.

GeneratePress - https://generatepress.com/
License: GNU General Public License (http://www.gnu.org/licenses/gpl.html) version 2.0 or later.

Font Awesome - https://fortawesome.github.io/Font-Awesome/
FontAwesome by Dave Gandy
Applies to all desktop and webfont files in the following directory: font-awesome/fonts/.
License: SIL OFL 1.1
URL: http://scripts.sil.org/OFL
Applies to all CSS and LESS files in the following directories: font-awesome/css/, font-awesome/less/, and font-awesome/scss/.
License: MIT License
URL: http://opensource.org/licenses/mit-license.html

Unsemantic Framework - https://unsemantic.com/
License: http://opensource.org/licenses/mit-license.php
License: http://www.gnu.org/licenses/gpl.html

classList By Eli Grey, http://eligrey.com
License: Dedicated to the public domain. https://github.com/eligrey/classList.js/blob/master/LICENSE.md

selectWoo
MIT License: https://github.com/woocommerce/selectWoo/blob/master/LICENSE.md

Google Fonts - https://www.google.com/fonts
Licensed under Apache License Version 2 (http://www.apache.org/licenses/LICENSE-2.0)

Images on screenshot:
Main image: https://pxhere.com/en/photo/1532231
Copyright pxhere
License: https://creativecommons.org/publicdomain/zero/1.0/

Content-1: https://pxhere.com/en/photo/1364511
Copyright pxhere
License: https://creativecommons.org/publicdomain/zero/1.0/

Dots and patterns
Copyright wpkoithemes
License: https://creativecommons.org/publicdomain/zero/1.0/

== Change Log ==

= 1.2.0 =
Added: accessibility features
Added: wp_body_open function
Fixed: better custom social icons
Fixed: category description
Changed: Pexels images on screenshot

= 1.1.1 =
Fixed: compatibility with PHP 7.3
Fixed: TGMPA issues

= 1.1.0 =
Fixed: Icon compatibility with newest Elementor
Changed: Demo URL
Added: WPKoi Templates for Elementor to the recommended plugins

= 1.0.0 =
First release