(function ( $ ) {
	'use strict';

	$( window ).load(
		function () {
			for ( var key in qodefCore.shortcodes ) {
				for ( var keyChild in qodefCore.shortcodes[key] ) {
					qodefElementor.init( key, keyChild );
				}
			}
		}
	);

	var qodefElementor = {
		init: function ( key, keyChild ) {
			$( window ).on(
				'elementor/frontend/init',
				function ( e ) {
					elementorFrontend.hooks.addAction(
						'frontend/element_ready/' + key + '.default',
						function ( e ) {
							// Check if object doesn't exist and print the module where is the error
							if ( typeof qodefCore.shortcodes[key][keyChild] === 'undefined' ) {
								console.log( keyChild );
							}

							qodefCore.shortcodes[key][keyChild].init();
						}
					);
				}
			);
		}
	};

})( jQuery );
