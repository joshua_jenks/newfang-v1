<?php

class QodeEssentialAddons_Portfolio_List_Shortcode_Elementor extends QodeEssentialAddons_Elementor_Widget_Base {

	function __construct( array $data = [], $args = null ) {
		$this->set_shortcode_slug( 'qode_essential_addons_portfolio_list' );

		parent::__construct( $data, $args );
	}
}

qode_essential_addons_get_elementor_widgets_manager()->register_widget_type( new QodeEssentialAddons_Portfolio_List_Shortcode_Elementor() );
