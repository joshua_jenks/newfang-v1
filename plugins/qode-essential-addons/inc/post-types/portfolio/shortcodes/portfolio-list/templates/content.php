<div <?php qode_essential_addons_framework_class_attribute( $holder_classes ); ?>>
	<div class="qodef-grid-inner">
		<?php
		// Include global masonry template from theme
		qode_essential_addons_template_part( 'masonry', 'templates/sizer-gutter', '', $params['behavior'] );

		// Include items
		qode_essential_addons_template_part( 'post-types/portfolio/shortcodes/portfolio-list', 'templates/loop', '', $params );
		?>
	</div>
	<?php
	// Include global pagination from theme
	qode_essential_addons_template_part( 'pagination', 'templates/pagination', 'standard', $params );
	?>
</div>
