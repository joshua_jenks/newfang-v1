<a itemprop="url" class="qodef-mobile-header-logo-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" <?php qode_essential_addons_framework_inline_style( $logo_height ); ?> rel="home">
	<?php
	// Include mobile header logo image html
	echo qode_essential_addons_framework_wp_kses_html( 'img', $logo_main_image );

	// Hook to include additional content after mobile header logo image
	do_action( 'qode_essential_addons_after_mobile_header_logo_image' );
	?>
</a>
