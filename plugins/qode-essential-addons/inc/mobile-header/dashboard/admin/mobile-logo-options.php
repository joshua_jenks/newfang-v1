<?php

if ( ! function_exists( 'qode_essential_addons_add_mobile_logo_options' ) ) {
	/**
	 * Function that add general options for this module
	 *
	 * @param object $page
	 * @param object $header_tab
	 */
	function qode_essential_addons_add_mobile_logo_options( $page, $header_tab ) {

		if ( $page ) {

			$mobile_header_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-mobile-header',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Mobile Header Logo Options', 'qode-essential-addons' ),
					'description' => esc_html__( 'Set options for mobile headers', 'qode-essential-addons' ),
				)
			);

			$mobile_header_tab->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_mobile_logo_height',
					'title'       => esc_html__( 'Mobile Logo Height', 'qode-essential-addons' ),
					'description' => esc_html__( 'Set a default height for mobile logo', 'qode-essential-addons' ),
					'args'        => array(
						'suffix' => esc_html__( 'px', 'qode-essential-addons' ),
					),
				)
			);

			$mobile_header_tab->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_mobile_logo_padding',
					'title'       => esc_html__( 'Mobile Logo Padding', 'qode-essential-addons' ),
					'description' => esc_html__( 'Input default values for mobile logo padding (top right bottom left)', 'qode-essential-addons' ),
				)
			);

			$mobile_header_tab->add_field_element(
				array(
					'field_type'    => 'image',
					'name'          => 'qodef_mobile_logo_main',
					'title'         => esc_html__( 'Mobile Logo', 'qode-essential-addons' ),
					'description'   => esc_html__( 'Choose main mobile logo image', 'qode-essential-addons' ),
					'default_value' => '',
					'multiple'      => 'no',
				)
			);

			do_action( 'qode_essential_addons_action_after_mobile_logo_options_map', $page );
		}
	}

	add_action( 'qode_essential_addons_action_after_header_logo_options_map', 'qode_essential_addons_add_mobile_logo_options', 10, 2 );
}
