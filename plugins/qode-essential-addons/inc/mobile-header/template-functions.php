<?php

if ( ! function_exists( 'qode_essential_addons_render_mobile_header_logo_image' ) ) {
	/**
	 * Function that print logo image html for current module
	 *
	 * @return string that contains html content
	 */
	function qode_essential_addons_render_mobile_header_logo_image() {
		echo qode_essential_addons_get_mobile_header_logo_image();
	}
}

if ( ! function_exists( 'qode_essential_addons_get_mobile_header_logo_image' ) ) {
	/**
	 * Function that return logo image html for current module
	 *
	 * @return string that contains html content
	 */
	function qode_essential_addons_get_mobile_header_logo_image() {
		$logo_image           = qode_essential_addons_get_header_logo_image();
		$mobile_image         = '';
		$mobile_logo_image_id = qode_essential_addons_get_post_value_through_levels( 'qodef_mobile_logo_main' );

		if ( ! empty( $mobile_logo_image_id ) ) {
			$logo_height_mobile  = qode_essential_addons_get_post_value_through_levels( 'qodef_mobile_logo_height' );
			$logo_padding_mobile = qode_essential_addons_get_post_value_through_levels( 'qodef_mobile_logo_padding' );
			$logo_height         = ! empty( $logo_height_mobile ) ? $logo_height_mobile : qode_essential_addons_get_post_value_through_levels( 'qodef_logo_height' );

			$logo_styles = array();
			if ( ! empty( $logo_height ) ) {
				$logo_styles[] = 'height:' . intval( $logo_height ) . 'px';
			}

			if ( ! empty( $logo_padding_mobile ) ) {
				$logo_styles[] = 'padding:' . esc_attr( $logo_padding_mobile );
			}

			$logo_main_image_attr = array(
				'class'    => 'qodef-header-logo-image qodef--main',
				'itemprop' => 'image',
				'alt'      => esc_attr__( 'logo main', 'qode-essential-addons' ),
			);

			$image      = wp_get_attachment_image( $mobile_logo_image_id, 'full', false, $logo_main_image_attr );
			$image_html = ! empty( $image ) ? $image : qode_essential_addons_framework_get_image_html_from_src( $mobile_logo_image_id, $logo_main_image_attr );

			$parameters = array(
				'logo_height'     => implode( ';', $logo_styles ),
				'logo_main_image' => $image_html,
			);

			$mobile_image = qode_essential_addons_get_template_part( 'mobile-header/templates', 'parts/mobile-logo', '', $parameters );
		} elseif ( ! empty( $logo_image ) ) {
			$mobile_image = $logo_image;
		}

		return $mobile_image;
	}
}

if ( ! function_exists( 'qode_essential_addons_set_mobile_header_logo_image' ) ) {
	/**
	 * Function that return logo image html for current module
	 *
	 * @param string $template - contains html content
	 *
	 * @return string that contains html content
	 */
	function qode_essential_addons_set_mobile_header_logo_image( $template ) {
		$mobile_image = qode_essential_addons_get_mobile_header_logo_image();

		if ( ! empty( $mobile_image ) ) {
			return $mobile_image;
		}

		return $template;
	}

	// @WPThemeHookList
	add_filter( 'the_two_filter_mobile_header_logo_template', 'qode_essential_addons_set_mobile_header_logo_image' );
	add_filter( 'the_q_filter_mobile_header_logo_template', 'qode_essential_addons_set_mobile_header_logo_image' );
	add_filter( 'qi_filter_mobile_header_logo_template', 'qode_essential_addons_set_mobile_header_logo_image' );
}
